#include <stdio.h>
int main(int argc, char const *argv[])
{
	int i = 1;
	int entrada;
	printf("Dame un numero mayor a 5\n");
	scanf("%d", &entrada);
	while ( i <= entrada)
	{
		if (i%3==0 && i%5==0)
		{
			printf("Fizz Buzz\n");
		}else if (i%3==0)
		{
			printf("Fizz\n");
		}else if (i%5==0)
		{
			printf("Buzz\n");
		}else{
			printf("%d\n", i);
		}
		 ++i;
	}
	return 0;
}